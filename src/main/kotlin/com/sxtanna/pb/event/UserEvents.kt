package com.sxtanna.pb.event

import com.sxtanna.diss.event.Act
import com.sxtanna.diss.type.IAct
import com.sxtanna.pb.user.User
import com.sxtanna.pb.user.rank.Rank
import kotlin.reflect.KClass

abstract class UserEvent(override val actType : KClass<out IAct>) : Act(actType) {

	abstract val user : User

}


class UserCreateEvent(override val user : User) : UserEvent(UserCreateEvent::class)

class UserGameDataLoadEvent(override val user : User) : UserEvent(UserGameDataLoadEvent::class)

class UserCustomDataLoadEvent(override val user : User) : UserEvent(UserCustomDataLoadEvent::class)

class UserRankChangeEvent(override val user : User, val old : Rank, val new : Rank) : UserEvent(UserRankChangeEvent::class)