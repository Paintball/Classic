package com.sxtanna.pb.event

import com.sxtanna.diss.event.Act
import com.sxtanna.diss.type.IAct
import com.sxtanna.pb.base.GameState
import com.sxtanna.pb.game.Game
import com.sxtanna.pb.map.Map
import kotlin.reflect.KClass

abstract class EventGame(override val actType : KClass<out IAct>) : Act(actType) {

	abstract val game: Game

}


class EventGameStateChange(override val game : Game, val old : GameState, val new : GameState) : EventGame(EventGameStateChange::class)

class EventGameMapChange(override val game : Game, val old : Map, val new : Map) : EventGame(EventGameMapChange::class) {

	val fromDefault : Boolean
		get() = Map.isDefault(old)

	val toDefault : Boolean
		get() = Map.isDefault(new)

}