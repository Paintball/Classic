package com.sxtanna.pb.kits

import com.sxtanna.pb.base.KitInfo
import com.sxtanna.pb.effect.SoundEffect
import com.sxtanna.pb.game.Game
import com.sxtanna.pb.kits.base.KitPaintball
import org.bukkit.Material.*
import org.bukkit.Sound.ENTITY_CHICKEN_EGG

class KitRifle(override val game : Game, override val damage : Double = 11.0) : KitPaintball() {

	override val info = rifleInfo
	override val sound = rifleSound

	override val speed = 3.5

	override val gunType = IRON_BARDING
	override val gunShots = 1


	companion object {

		private val rifleInfo = KitInfo("Rifle", listOf("Two shot kill"))
		private val rifleSound = SoundEffect(ENTITY_CHICKEN_EGG, 1.5, 1.4)

	}

}