package com.sxtanna.pb.kits.base

import com.sxtanna.ext.isIn
import com.sxtanna.pb.effect.SoundEffect
import com.sxtanna.pb.ext.direction
import com.sxtanna.pb.ext.showDust
import com.sxtanna.pb.ext.spawn
import com.sxtanna.pb.ext.syncRepeat
import com.sxtanna.pb.kit.global.KitTeamArmour
import com.sxtanna.pb.user.User
import com.sxtanna.pb.util.Colour
import com.sxtanna.pb.util.base.Meta
import org.bukkit.Material
import org.bukkit.entity.Snowball
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority.HIGHEST
import org.bukkit.event.block.Action.RIGHT_CLICK_AIR
import org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.util.Vector

abstract class KitPaintball : KitTeamArmour() {

	open val speed : Double = 1.1

	abstract val gunType : Material
	abstract val gunShots : Int

	abstract val damage : Double
	abstract val sound : SoundEffect


	open fun stray() : Vector? = null

	protected fun shoot(user : User) : Snowball {
		val player = user.player
		val dir = direction(player)
		val colour = Colour.Red //game.teamOf(user).colour

		return Snowball::class.spawn(player.eyeLocation.add(dir.clone().multiply(1.3))).apply {
			shooter = player
			velocity = dir.clone().multiply(speed)

			Meta[this, "Paintball"] = damage

			syncRepeat(1) {
				if (isValid.not()) return@syncRepeat cancel()
				showDust(location, colour)
			}
		}
	}


	@EventHandler(priority = HIGHEST)
	fun onShootGun(e : PlayerInteractEvent) {
		val user = game.plugin.userManager[e.player]
		if (e.action.isIn(RIGHT_CLICK_AIR, RIGHT_CLICK_BLOCK).not() || e.hasItem().not() || e.item.type != gunType || user !in this) return

		for (i in gunShots downTo 1) {
			val bullet = shoot(user)
			val stray = stray() ?: continue

			bullet.apply {
				velocity = velocity.add(stray)
			}
		}

		sound.sendNear(e.player.location, 16)
	}




}