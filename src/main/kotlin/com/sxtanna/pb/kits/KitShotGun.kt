package com.sxtanna.pb.kits

import com.sxtanna.pb.base.KitInfo
import com.sxtanna.pb.effect.SoundEffect
import com.sxtanna.pb.ext.double
import com.sxtanna.pb.game.Game
import com.sxtanna.pb.kits.base.KitPaintball
import org.bukkit.Material.*
import org.bukkit.Sound.ENTITY_GENERIC_EXPLODE
import org.bukkit.util.Vector

class KitShotGun(override val game : Game, override val damage : Double = 5.0) : KitPaintball() {

	override val info = shotGunInfo
	override val sound = shotGunSound

	override val speed = 1.6

	override val gunType = GOLD_BARDING
	override val gunShots = 8

	override fun stray() : Vector = Vector(double(1.5) - 0.6, double(1.0) - 0.5, double(1.5) - 0.6).multiply(0.4)


	companion object {

		private val shotGunInfo = KitInfo("ShotGun", listOf("8 Shots, 4 to kill"))
		private val shotGunSound = SoundEffect(ENTITY_GENERIC_EXPLODE, 0.7, 2.0)

	}

}