package com.sxtanna.pb.cmd

import com.sxtanna.pb.Paintball
import com.sxtanna.pb.base.Manager
import com.sxtanna.pb.cmd.cmds.GameCmd
import org.bukkit.command.CommandSender
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority.HIGHEST
import org.bukkit.event.player.PlayerCommandPreprocessEvent

class CommandManager(override val plugin : Paintball) : Manager() {

	private val commands = mutableMapOf<String, Command>()


	override fun onEnable() {
		register(GameCmd(plugin))
	}

	override fun onDisable() = Unit

	operator fun get(name: String): Command? = commands[name.toLowerCase()]

	fun register(vararg cmds : Command) {
		cmds.forEach { cmd -> cmd.aliases.forEach { commands[it.toLowerCase()] = cmd } }
	}


	private fun handle(sender : CommandSender, message: String) : Boolean {
		val arguments = message.split(' ')

		var name = arguments[0].toLowerCase()
		if (name.startsWith('/')) name = name.substring(1)

		val target = this[name] ?: return false

		val cmdArguments = arguments.subList(1, arguments.size)
		target.invoke(sender, name, cmdArguments)
		return true
	}


	@EventHandler(priority = HIGHEST, ignoreCancelled = true)
	fun onCmd(e: PlayerCommandPreprocessEvent) {

	}



}