package com.sxtanna.pb.cmd

import com.sxtanna.pb.type.ReqPlugin
import org.bukkit.command.CommandSender

abstract class Command(val name : String, vararg val aliases : String) : ReqPlugin {

	abstract operator fun invoke(sender : CommandSender, alias: String, args : List<String>)

}