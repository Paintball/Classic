package com.sxtanna.pb.user

import com.sxtanna.diss.Vega
import com.sxtanna.pb.base.GameAspect
import com.sxtanna.pb.base.UserState
import com.sxtanna.pb.event.UserRankChangeEvent
import com.sxtanna.pb.ext.async
import com.sxtanna.pb.ext.keury
import com.sxtanna.pb.user.rank.Rank
import com.sxtanna.struct.Where.Equals
import net.md_5.bungee.api.ChatMessageType.ACTION_BAR
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.Bukkit
import java.util.*

class User(val uuid : UUID) {

	private val userData = mutableMapOf<String, String>()
	private val aspectData = mutableMapOf<GameAspect, Double>()

	var state = UserState.Offline
	var rank = Rank.Player
	set(value) {
		Vega.push(UserRankChangeEvent(this, field, value))
		field = value

		async(keury) {
			update("Users", arrayOf("Rank" to value), Equals("ID", uuid))
		}
	}

	val player by lazy { checkNotNull(Bukkit.getPlayer(uuid)) { "Player $uuid isn't online" } }


	operator fun get(aspect : GameAspect) = aspectData.getOrPut(aspect) { 0.0 }

	operator fun set(aspect : GameAspect, value : Double) {
		aspectData[aspect] = value
	}

	operator fun get(dataKey : String) = userData[dataKey]

	operator fun set(dataKey : String, any : Any) {
		userData[dataKey] = any.toString()
	}


	fun data(): Map<String, String> = userData

	fun aspects(): Map<GameAspect, Double> = aspectData


	fun GameAspect.inc(amount : Double = 1.0) : Double {
		return aspectData.compute(this) { _, v -> v?.let { it + amount } ?: 0.0 }!!
	}


	fun chat(message : String) = player.sendMessage(message)

	fun actionBar(message : String) = player.spigot().sendMessage(ACTION_BAR, TextComponent(message))

	fun title(title: String, subtitle: String = "", stay: Int = 70, fadeIn: Int = 10, fadeOut: Int = 20) {
		player.sendTitle(title, subtitle, fadeIn, stay, fadeOut)
	}


	override fun equals(other : Any?) : Boolean {
		if (this === other) return true
		if (other !is User) return false

		if (uuid != other.uuid) return false

		return true
	}

	override fun hashCode() : Int {
		return uuid.hashCode()
	}


}