package com.sxtanna.pb.user.rank

enum class Rank {

	Player,
	Moderator,
	Admin,
	Owner

}