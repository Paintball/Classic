package com.sxtanna.pb.user

import com.sxtanna.diss.Vega
import com.sxtanna.ext.ALL_ROWS
import com.sxtanna.pb.Paintball
import com.sxtanna.pb.base.GameAspect
import com.sxtanna.pb.base.Manager
import com.sxtanna.pb.event.UserCreateEvent
import com.sxtanna.pb.event.UserCustomDataLoadEvent
import com.sxtanna.pb.event.UserGameDataLoadEvent
import com.sxtanna.pb.ext.*
import com.sxtanna.pb.kits.KitShotGun
import com.sxtanna.pb.user.rank.Rank
import com.sxtanna.pb.util.DRed
import com.sxtanna.pb.util.Red
import com.sxtanna.struct.Duplicate.Update
import com.sxtanna.struct.SqlType.*
import com.sxtanna.struct.SqlType.Char
import com.sxtanna.struct.SqlType.EnumSet
import com.sxtanna.struct.Where.Equals
import org.bukkit.entity.Player
import org.bukkit.event.player.AsyncPlayerPreLoginEvent
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result.ALLOWED
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result.KICK_OTHER
import java.util.*

class UserManager(override val plugin : Paintball) : Manager() {

	val users = mutableMapOf<UUID, User>()
	val shotGun = KitShotGun(plugin.classic.game)

	val players = mutableListOf<Player>().apply {
		forEachOnline { add(this) }
	}

	override fun onEnable() {
		shotGun.enable()

		async(keury) {
			val columns = Array(aspects.size) { aspects[it].name to Decimal(20, 2) }
			createTable("Users", "ID" to Char(36, true), "Rank" to EnumSet(Rank::class), *columns)

			forEachOnline {
				load(uniqueId)
			}
		}
	}

	override fun onDisable() {
		async {
			forEachOnline {
				unload(uniqueId)
			}
		}
	}


	operator fun get(uuid : UUID): User = checkNotNull(users[uuid]) { "User $uuid not found" }

	operator fun get(player : Player): User = get(player.uniqueId)


	operator fun get(uuid : UUID, load: Boolean = false, block: User.() -> Unit) {
		users[uuid]?.let(block) ?: return load.ifTrue { async { block(load(uuid)) }  }
	}

	operator fun get(player: Player, block : User.() -> Unit) = get(player.uniqueId, false, block)


	/**
	 * Only call from an Async Context
	 */
	private fun load(uuid : UUID) : User {
		val user = User(uuid)
		Vega.push(UserCreateEvent(user))

		keury {
			select("Users", ALL_ROWS, arrayOf(Equals("ID", uuid)), true) {

				user.rank = Rank.valueOf(getString("Rank"))

				val meta = metaData
				val columns = Array(meta.columnCount - 2) { meta.getColumnName(it + 3) }

				columns.forEach {
					user[GameAspect.valueOf(it)] = getDouble(it)
				}

				Vega.push(UserGameDataLoadEvent(user))
			}
		}

		kedis {
			getHash("users:$uuid:data").forEach { key, data -> user[key] = data }
			Vega.push(UserCustomDataLoadEvent(user))
		}

		users[uuid] = user

		plugin.classic.game.apply {
			chosenKits[user] = shotGun
		}

		return user
	}

	/**
	 * Only call from an Async Context
	 */
	private fun unload(uuid : UUID) {
		val user = users.remove(uuid) ?: return

		kedis {
			user.data().forEach { key, data -> setHash("users:$uuid", key, data) }
		}

		keury {
			val columns = Array(aspects.size) {
				val aspect = aspects[it]
				aspect.name to user[aspect]
			}

			insert("Users", arrayOf("ID" to uuid, *columns), Update(*aspectNames))
		}
	}


	override fun onPreJoin(event : AsyncPlayerPreLoginEvent) {
		val config = plugin.config
		val baseKey = "users:${event.uniqueId}"

		kedis {

			val dataKey = "$baseKey:data"
			val permsKey = "$baseKey:perms"

			if (contains(dataKey).not()) {
				setHash(dataKey, "nothing", "nothing")
				setSet(permsKey, "nothing")
			}

			if (config.access.restricted) {
				if (hasSet(permsKey, config.access.userPerm).not()) {
					event.disallow(KICK_OTHER, "${Red}You aren't allowed on this server $DRed${config.serverName}")
				}
			}

			if (event.loginResult == ALLOWED) load(event.uniqueId)

		}
	}

	override fun onQuit(player : Player) {
		async { unload(player.uniqueId) }
	}


	companion object {

		private val aspects = GameAspect.values()
		private val aspectNames = aspects.map { it.name }.toTypedArray()

	}

}