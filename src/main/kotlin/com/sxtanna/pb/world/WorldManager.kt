package com.sxtanna.pb.world

import com.sxtanna.pb.Paintball
import com.sxtanna.pb.ext.serverDir
import com.sxtanna.pb.type.ReqPlugin
import com.sxtanna.pb.type.Switch
import org.bukkit.Bukkit
import org.bukkit.World
import org.bukkit.WorldCreator
import java.io.File
import java.lang.ref.SoftReference

class WorldManager(override val plugin : Paintball) : Switch, ReqPlugin {


	lateinit var lobbyWorld : SoftReference<World>
		private set


	override fun onEnable() {
		lobbyWorld = SoftReference(Bukkit.getWorlds()[0])
	}

	override fun onDisable() {
		lobbyWorld.clear()
	}


	fun loadWorld(name : String) : World {
		val loaded = Bukkit.getWorld(name)
		if (loaded != null) return loaded

		check(File(serverDir(), name).exists()) { "World $name does not exist" }

		return WorldCreator(name).createWorld()
	}

	fun unloadWorld(world : World) {
		lobbyWorld.get()?.apply {
			world.players.forEach { it.teleport(this.spawnLocation) }
		}


		Bukkit.unloadWorld(world, false)
	}


}