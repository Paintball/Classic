package com.sxtanna.pb.base

import org.bukkit.Sound
import org.bukkit.util.Vector


data class GameInfo(val name: String, val acronym: String, val desc: List<String>)

data class KitInfo(val name: String, val desc: List<String>)

data class MapInfo(val name: String, val creator: String, val worldInfo : WorldInfo, val spawnsInfo : SpawnsInfo) {

	data class WorldInfo(val name: String, val min: Vector, val max: Vector)

	data class SpawnsInfo(val spawnPoints: Map<Int, List<Vector>>, val dataPoints: Map<Int, List<Vector>>)


	fun toMap() = com.sxtanna.pb.map.Map(this)

}

data class SoundData(val sound : Sound, val volume : Double = 1.0, val pitch : Double = 1.0)


enum class UserState {

	Offline,
	Online,

	Alive,
	Dead,

	Spec

}


enum class GameState {

	Init,

	Loaded,
	Countdown,
	Started,
	InGame,
	Ended,
	Resetting;

}


enum class GameAspect {

	Kills,
	Assists,
	Deaths,

	DamageDealt,
	DamageTaken,

	Wins,
	Loses,

	ShotsFired,
	ShotsTaken,

	PotionsThrown,
	PotionsUsed

}