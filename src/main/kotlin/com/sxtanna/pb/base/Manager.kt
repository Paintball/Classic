package com.sxtanna.pb.base

import com.sxtanna.pb.ext.register
import com.sxtanna.pb.ext.unregister
import com.sxtanna.pb.type.ReqGson
import com.sxtanna.pb.type.ReqJoinQuit
import com.sxtanna.pb.type.ReqPlugin
import org.bukkit.entity.Player
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerPreLoginEvent

abstract class Manager : ReqJoinQuit, ReqPlugin, ReqGson, Listener {

	override fun enable() {
		super.enable()
		register(plugin)
	}

	override fun disable() {
		super.disable()
		unregister()
	}


	override fun onPreJoin(event : AsyncPlayerPreLoginEvent) = Unit

	override fun onJoin(player : Player) = Unit

	override fun onQuit(player : Player) = Unit

}