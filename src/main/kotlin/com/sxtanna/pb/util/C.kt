package com.sxtanna.pb.util

fun M(color: Cl): String = "$color$Magic"

fun B(color: Cl): String = "$color$Bold"

fun S(color: Cl): String = "$color$Strike"

fun U(color: Cl): String = "$color$ULined"

fun I(color: Cl): String = "$color$Italic"


data class Cl(val char: Char) {
    override fun toString(): String = "$COLOR_CODE$char"
}


val Black  = Cl('0')
val DBlue  = Cl('1')
val DGreen = Cl('2')
val DAqua  = Cl('3')
val DRed   = Cl('4')
val Violet = Cl('5')
val Gold   = Cl('6')
val Gray   = Cl('7')
val DGray  = Cl('8')
val Indigo = Cl('9')

val Green  = Cl('a')
val Aqua   = Cl('b')
val Red    = Cl('c')
val Pink   = Cl('d')
val Yellow = Cl('e')
val White  = Cl('f')

val Magic  = Cl('k')
val Bold   = Cl('l')
val Strike = Cl('m')
val ULined = Cl('n')
val Italic = Cl('o')
val Reset  = Cl('r')

val Line = "\n"

val Msg    = Gray
val Value  = Yellow
val Name   = Indigo
val Option = Gold

const val COLOR_CODE = '§'