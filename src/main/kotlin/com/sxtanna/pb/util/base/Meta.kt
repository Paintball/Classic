package com.sxtanna.pb.util.base

import com.sxtanna.pb.Paintball
import org.bukkit.metadata.FixedMetadataValue
import org.bukkit.metadata.MetadataValue
import org.bukkit.metadata.Metadatable
import org.bukkit.plugin.Plugin

object Meta {

	private val plugin : Plugin
		get() = Paintball.instance


	operator fun get(meta : Metadatable, name : String) : MetadataValue? = getAll(meta, name).firstOrNull { it.owningPlugin == plugin }

	operator fun set(meta : Metadatable, name : String, value : Any) = meta.setMetadata(name, FixedMetadataValue(plugin, value))


	fun rem(meta : Metadatable, name : String) = meta.removeMetadata(name, plugin)

	fun has(meta : Metadatable, name : String) : Boolean = meta.hasMetadata(name)

	fun getAll(meta : Metadatable, name : String) : List<MetadataValue> = meta.getMetadata(name)

}