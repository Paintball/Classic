package com.sxtanna.pb.util.base

import org.bukkit.entity.LivingEntity
import org.bukkit.potion.PotionEffect
import org.bukkit.potion.PotionEffectType
import org.bukkit.potion.PotionEffectType.*

object Condition {


	operator fun get(livingEntity : LivingEntity, type : Type, time: Int, level: Int = 1, ambient: Boolean = false, particles: Boolean = true, force: Boolean = true) {
		val effect = PotionEffect(type.effectType, time, if (level >= 1) level -1 else 0, ambient, particles)
		livingEntity.addPotionEffect(effect, force)
	}

	operator fun get(livingEntity : LivingEntity, vararg type : Type) {
		type.forEach { livingEntity.removePotionEffect(it.effectType) }
	}


	enum class Type(val effectType : PotionEffectType) {

		Speed        (SPEED),
		Slow         (SLOW),
		Haste        (FAST_DIGGING),
		MineFatigue  (SLOW_DIGGING),
		Strength     (INCREASE_DAMAGE),
		Healing      (HEAL),
		Harming      (HARM),
		JumpBoost    (JUMP),
		Nausea       (CONFUSION),
		Regen        (REGENERATION),
		Resistance   (DAMAGE_RESISTANCE),
		FireResist   (FIRE_RESISTANCE),
		WaterBreath  (WATER_BREATHING),
		Invisibility (INVISIBILITY),
		Blindness    (BLINDNESS),
		NightVision  (NIGHT_VISION),
		Hunger       (HUNGER),
		Weakness     (WEAKNESS),
		Poison       (POISON),
		Wither       (WITHER),
		HealthBoost  (HEALTH_BOOST),
		Absorption   (ABSORPTION),
		Saturation   (SATURATION);

	}

}