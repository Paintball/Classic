package com.sxtanna.pb.util

import org.bukkit.ChatColor
import org.bukkit.ChatColor.*
import org.bukkit.DyeColor

typealias BukkitColor = org.bukkit.Color

enum class Colour
(val chatColor: ChatColor, val color: BukkitColor, val dyeColor: DyeColor, val char: Char, val clr: Cl, val jsonName: String = chatColor.name.toLowerCase()) {

    /**
     * Represents The Color **Black**
     */
    Black  (BLACK       , BukkitColor.BLACK  , DyeColor.BLACK     , '0', com.sxtanna.pb.util.Black),
    /**
     * Represents The Color **Dark Blue**
     */
    DBlue  (DARK_BLUE   , BukkitColor.BLUE   , DyeColor.BLUE      , '1', com.sxtanna.pb.util.DBlue),
    /**
     * Represents The Color **Dark Green**
     */
    DGreen (DARK_GREEN  , BukkitColor.GREEN  , DyeColor.GREEN     , '2', com.sxtanna.pb.util.DGreen),
    /**
     * Represents The Color **Dark Aqua**
     */
    DAqua  (DARK_AQUA   , BukkitColor.TEAL   , DyeColor.CYAN      , '3', com.sxtanna.pb.util.DAqua),
    /**
     * Represents The Color **Dark Red**
     */
    DRed   (DARK_RED    , BukkitColor.MAROON , DyeColor.RED       , '4', com.sxtanna.pb.util.DRed),
    /**
     * Represents The Color **Dark Purple**
     */
    Violet (DARK_PURPLE , BukkitColor.PURPLE , DyeColor.PURPLE    , '5', com.sxtanna.pb.util.Violet),
    /**
     * Represents The Color **Gold**
     */
    Gold   (GOLD        , BukkitColor.ORANGE , DyeColor.ORANGE    , '6', com.sxtanna.pb.util.Gold),
    /**
     * Represents The Color **Gray**
     */
    Gray   (GRAY        , BukkitColor.SILVER , DyeColor.SILVER    , '7', com.sxtanna.pb.util.Gray),
    /**
     * Represents The Color **Dark Gray**
     */
    DGray  (DARK_GRAY   , BukkitColor.GRAY   , DyeColor.GRAY      , '8', com.sxtanna.pb.util.DGray),
    /**
     * Represents The Color **Blue**
     */
    Blue   (BLUE        , BukkitColor.NAVY   , DyeColor.CYAN      , '9', Indigo),
    /**
     * Represents The Color **Green**
     */
    Green  (GREEN       , BukkitColor.LIME   , DyeColor.LIME      , 'a', com.sxtanna.pb.util.Green),
    /**
     * Represents The Color **Aqua**
     */
    Aqua   (AQUA        , BukkitColor.AQUA   , DyeColor.LIGHT_BLUE, 'b', com.sxtanna.pb.util.Aqua),
    /**
     * Represents The Color **Red**
     */
    Red    (RED         , BukkitColor.RED    , DyeColor.PINK      , 'c', com.sxtanna.pb.util.Red),
    /**
     * Represents The Color **Light Purple**
     */
    Purple (LIGHT_PURPLE, BukkitColor.FUCHSIA, DyeColor.MAGENTA   , 'd', Pink),
    /**
     * Represents The Color **Yellow**
     */
    Yellow (YELLOW      , BukkitColor.YELLOW , DyeColor.YELLOW    , 'e', com.sxtanna.pb.util.Yellow),
    /**
     * Represents The Color **White**
     */
    White  (WHITE       , BukkitColor.WHITE  , DyeColor.WHITE     , 'f', com.sxtanna.pb.util.White);


    companion object {

        operator fun get(chatColor: ChatColor): Colour {
            return values().find { it.chatColor == chatColor }?: White
        }

        operator fun get(bukkitColor: BukkitColor): Colour {
            return values().find { it.color == bukkitColor }?: White
        }

        operator fun get(dyeColor: DyeColor): Colour {
            return values().find { it.dyeColor == dyeColor }?: White
        }

        operator fun get(name: String): Colour {
            return values().find { it.name.equals(name, true) || it.jsonName.equals(name, true) }?: White
        }

    }

}