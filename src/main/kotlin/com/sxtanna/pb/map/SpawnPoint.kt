package com.sxtanna.pb.map

import org.bukkit.util.Vector

class SpawnPoint(val vector : Vector, var priority: Int = 0) : Comparable<SpawnPoint> {


	operator fun inc(): SpawnPoint {
		++priority
		return this
	}

	override fun compareTo(other : SpawnPoint) : Int {
		val compare = priority.compareTo(other.priority)
		return if (compare == 0) 1 else compare
	}
}