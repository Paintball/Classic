package com.sxtanna.pb.map

import com.sxtanna.KUtils
import com.sxtanna.KUtils.Files
import com.sxtanna.pb.base.GameInfo
import com.sxtanna.pb.base.MapInfo
import com.sxtanna.pb.ext.random
import com.sxtanna.pb.ext.serverDir
import com.sxtanna.pb.map.SaveResult.FailWorld
import com.sxtanna.pb.type.Switch
import org.apache.commons.io.FileUtils
import org.bukkit.World
import org.bukkit.WorldCreator
import java.io.File
import java.io.FileReader
import java.io.IOException

class MapProvider(val info: GameInfo, val mapsFolder: File = File(serverDir(), info.acronym)) : Switch {

	private val maps = mutableListOf<MapInfo>()
	private var lastMap = ""


	override fun onEnable() {
		mapsFolder.mkdirs()

		try {
			val worldFolders = mapsFolder.listFiles { path -> path.isDirectory } ?: return

			worldFolders
					.map { File(it, DATA_FILE) }
					.filterNot { it.exists().not() }
					.mapNotNullTo(maps) { dataFile -> FileReader(dataFile).use { KUtils.gson.fromJson(it, MapInfo::class.java) } }
		}
		catch (e: Exception) {
			e.printStackTrace()
		}

		maps.sortBy { it.name }
	}

	override fun onDisable() {
		maps.clear()
	}


	fun mapNames() = maps.map { it.name }

	fun hasMaps() = maps.isNotEmpty()

	fun findMap(name: String): MapInfo? = maps.find { it.name.equals(name, true) }

	fun randomMap(): Map? {
		if (hasMaps().not()) return null

		var random: MapInfo? = null

		if (maps.size == 1) random = maps.first()
		else {
			while (random == null || (lastMap == random.name)) random = maps.random()
			lastMap = random.name
		}

		val map = random.toMap()
		loadWorld(random) {
			map.setWorld(checkNotNull(it) { "Cannot set world of Map" })
		}

		return map
	}

	fun loadWorld(info : MapInfo, result: (World?) -> Unit) {
		try {
			FileUtils.deleteDirectory(File(serverDir(), info.worldInfo.name))
			FileUtils.copyDirectoryToDirectory(File(mapsFolder, info.worldInfo.name), serverDir())

			result.invoke(WorldCreator(info.worldInfo.name).createWorld())
		}
		catch (e: IOException) {
			println("Failed to load World ${info.worldInfo.name}")
			e.printStackTrace()

			result.invoke(null)
		}
	}

	fun saveMap(map : Map, result: (SaveResult) -> Unit) {
		val world = checkNotNull(map.world.get()) { result.invoke(FailWorld); "World for map ${map.info.name} not found" }
		val info = map.info

		val worldFolder = world.worldFolder
		val saveFolder = File(mapsFolder, info.worldInfo.name)

		Files.saveToFile(File(saveFolder, DATA_FILE), info)

		FileUtils.copyFileToDirectory(File(worldFolder, "level.dat"), saveFolder)
		FileUtils.copyDirectoryToDirectory(File(worldFolder, "region"), saveFolder)
	}


	companion object {

		const val DATA_FILE = "data.json"

	}

}