package com.sxtanna.pb.map

enum class SaveResult {

	FailWorld,
	FailCopy,
	Success

}