package com.sxtanna.pb.map

import com.sxtanna.pb.base.MapInfo
import com.sxtanna.pb.base.MapInfo.SpawnsInfo
import com.sxtanna.pb.base.MapInfo.WorldInfo
import org.bukkit.World
import org.bukkit.util.Vector
import java.lang.ref.SoftReference

class Map(val info: MapInfo) {

	lateinit var world: SoftReference<World>
		private set


	fun setWorld(world: World) {
		this.world = SoftReference(world)
	}

	fun spawnsAt(index: Int): List<Vector> {
		val spawns = info.spawnsInfo.spawnPoints[index]
		return requireNotNull(spawns) { "No spawns for index $index" }
	}

	fun dataAt(index : Int): List<Vector> {
		val data = info.spawnsInfo.dataPoints[index]
		return requireNotNull(data) { "No data for index $index" }
	}


	override fun equals(other : Any?) : Boolean {
		if (this !== other || other !is Map) return false
		if (info != other.info) return false

		return true
	}

	override fun hashCode() : Int = info.hashCode()


	companion object {

		private val mapInfo = MapInfo("Default", "Sxtanna", WorldInfo("Unknown", Vector(0, 0, 0), Vector(0, 0, 0)), SpawnsInfo(emptyMap(), emptyMap()))
		val DEFAULT = Map(mapInfo)


		fun isDefault(map: Map) = map == DEFAULT

	}

}