package com.sxtanna.pb.game.classic

import com.sxtanna.pb.Paintball
import com.sxtanna.pb.base.GameInfo
import com.sxtanna.pb.ext.int
import com.sxtanna.pb.game.Game
import com.sxtanna.pb.kit.Kit
import com.sxtanna.pb.kits.KitRifle
import com.sxtanna.pb.kits.KitShotGun
import com.sxtanna.pb.team.Team
import com.sxtanna.pb.util.Colour.*

open class ClassicPaintball(override val plugin : Paintball, override val info : GameInfo = classicInfo) : Game() {

	override fun checkStart() : Pair<Boolean, String> {
		TODO("Return false or true based on amount of players")
	}

	override fun createKits() : List<Kit> {
		return listOf(KitRifle(this), KitShotGun(this))
	}

	override fun createTeams() : List<Team> {
		val index = int(0, teamOneNames.size)

		val teamOneName = teamOneNames[index]
		val teamTwoName = teamTwoNames[index]

		val teamOneColor = teamOneColors[index]
		val teamTwoColor = teamTwoColors[index]

		return listOf(Team(teamOneName, teamOneColor, this), Team(teamTwoName, teamTwoColor, this))
	}




	companion object {

		val classicInfo = GameInfo("Paintball Paintball", "PB", listOf("Regular Paintball", "Two Teams"))


		private val teamOneNames = arrayOf("Red", "Fire", "Nether", "Hell")
		private val teamTwoNames = arrayOf("Blue", "Ice", "Frost", "Heaven")

		private val teamOneColors = arrayOf(Red, Red, Red, DRed)
		private val teamTwoColors = arrayOf(Blue, Aqua, Aqua, Aqua)

	}

}