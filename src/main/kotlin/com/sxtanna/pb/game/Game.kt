package com.sxtanna.pb.game

import com.sxtanna.diss.BukkitSchedulerCont
import com.sxtanna.diss.SyncContext.Sync
import com.sxtanna.diss.Vega
import com.sxtanna.diss.task
import com.sxtanna.pb.base.GameInfo
import com.sxtanna.pb.base.GameState
import com.sxtanna.pb.base.GameState.*
import com.sxtanna.pb.effect.SoundEffect
import com.sxtanna.pb.event.EventGameMapChange
import com.sxtanna.pb.event.EventGameStateChange
import com.sxtanna.pb.ext.register
import com.sxtanna.pb.ext.unregister
import com.sxtanna.pb.kit.Kit
import com.sxtanna.pb.map.Map
import com.sxtanna.pb.team.Team
import com.sxtanna.pb.type.GameLogic
import com.sxtanna.pb.type.ReqJoinQuit
import com.sxtanna.pb.type.ReqPlugin
import com.sxtanna.pb.user.User
import com.sxtanna.pb.util.Gold
import com.sxtanna.pb.util.Gray
import com.sxtanna.pb.util.Green
import com.sxtanna.pb.util.base.Condition
import com.sxtanna.pb.util.base.Condition.Type.Blindness
import org.bukkit.Sound.BLOCK_NOTE_PLING
import org.bukkit.Sound.ENTITY_PLAYER_LEVELUP
import org.bukkit.entity.Player
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerPreLoginEvent

abstract class Game : ReqJoinQuit, ReqPlugin, GameLogic, Listener, Comparable<Game> {

	abstract val info: GameInfo

	var state = GameState.Init
		private set(value) {
			Vega.push(EventGameStateChange(this, field, value))
			field = value
		}

	var map : Map = Map.DEFAULT
		private set(value) {
			Vega.push(EventGameMapChange(this, field, value))
			field = value
		}


	val users = mutableListOf<User>()

	val kits = mutableListOf<Kit>()
	val teams = mutableListOf<Team>()

	val chosenKits = mutableMapOf<User, Kit>()
	val chosenTeams = mutableMapOf<User, Team>()

	val teamQueues = mutableMapOf<Team, MutableList<User>>()


	override fun prepare() {
		kits.addAll(createKits())
		teams.addAll(createTeams())

		state = Loaded
	}

	override fun countdown() {
		state = Countdown

		task(Sync) {

			countdown(10) { time ->
				users.forEach { it.title("${Gold}Game Starting in...", "$Gray${time}s") }
			}

			users.forEach {
				moveToMap(it)
				Condition[it.player, Blindness, 10 * 20, 10, true, false, true]

				mapWelcome.send(it.player)
			}
			start()
		}
	}

	override fun start() {
		state = Started

		task(Sync) {

			countdown(10) { time ->
				users.forEach { it.actionBar("${Gold}Game Starting in...$Green $time") }
			}

			users.forEach { Condition[it.player, Blindness] }

			state = InGame
		}
	}

	override fun end() {
		state = Ended
		TODO("not implemented")
	}

	override fun reset() {
		state = Resetting
		kits.forEach(Kit::disable).also { kits.clear() }
		teams.forEach(Team::clean).also { teams.clear() }

		state = Init
	}

	override fun moveToMap(user : User) {
		user.player.apply {

		}
	}

	override fun moveToLobby(user : User) {
		user.player.apply {

		}
	}

	override fun checkEnd() {
		TODO("not implemented")
	}


	override fun onEnable() {
		register(plugin)
	}

	override fun onDisable() {
		unregister()
	}


	override fun onPreJoin(event : AsyncPlayerPreLoginEvent) = Unit

	override fun onJoin(player : Player) {

	}

	override fun onQuit(player : Player) {

	}


	protected abstract fun createKits(): List<Kit>

	protected abstract fun createTeams(): List<Team>


	fun teamOf(user : User): Team = checkNotNull(chosenTeams[user]) { "User $user has no team" }

	fun kitOf(user : User): Kit = checkNotNull(chosenKits[user]) { "User $user has no kit" }


	protected suspend fun BukkitSchedulerCont.countdown(time : Int, period : Int = 20, stateLock: GameState = state, block: (Int) -> Unit) {
		for (i in time downTo 0) {
			block(i)
			waitFor(period.toLong())
			if (state != stateLock) return
		}
	}


	override fun equals(other : Any?) : Boolean {
		if (this !== other || other !is Game) return false
		if (info != other.info) return false

		return true
	}

	override fun hashCode() : Int = info.hashCode()

	override fun compareTo(other : Game) : Int = info.name.compareTo(other.info.name)


	companion object {

		val dingLow = SoundEffect(BLOCK_NOTE_PLING, 3.0, 0.8)
		val dingHigh = dingLow.copy(pitch = 1.6)

		val mapWelcome = SoundEffect(ENTITY_PLAYER_LEVELUP, 5.0, 1.8)

	}

}