package com.sxtanna.pb.type

interface Switch {

	val isEnabled: Boolean
		get() = this in switches


	fun enable() {
		check(switches.add(this)) { "This is already enabled" }

		onEnable()
	}

	fun disable() {
		check(switches.remove(this)) { "This is already disabled" }

		onDisable()
	}


	fun onEnable()

	fun onDisable()


	companion object {

		private val switches = hashSetOf<Switch>()

	}

}