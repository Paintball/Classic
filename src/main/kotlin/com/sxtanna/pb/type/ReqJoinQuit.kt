package com.sxtanna.pb.type

import org.bukkit.entity.Player
import org.bukkit.event.player.AsyncPlayerPreLoginEvent

interface ReqJoinQuit : Switch {

	override fun enable() {
		super.enable()
		joinQuits.add(this)
	}

	override fun disable() {
		super.disable()
		joinQuits.remove(this)
	}


	fun onPreJoin(event : AsyncPlayerPreLoginEvent)

	fun onJoin(player: Player)

	fun onQuit(player: Player)


	companion object {

		private val joinQuits = mutableSetOf<ReqJoinQuit>()


		fun pushJoin(player : Player) = joinQuits.forEach { it.onJoin(player) }

		fun pushQuit(player : Player) = joinQuits.forEach { it.onQuit(player) }

	}

}