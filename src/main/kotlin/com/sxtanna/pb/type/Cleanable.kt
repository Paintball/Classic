package com.sxtanna.pb.type

interface Cleanable {

	fun clean()

}