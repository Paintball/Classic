package com.sxtanna.pb.type

import com.sxtanna.pb.ext.forEachOnline
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.command.CommandSender

interface Sendable<out S> {

	val sending: S


	fun <C : CommandSender> send(vararg receivers: C): Unit


	fun sendAll() {
		forEachOnline { send(this) }
		send(Bukkit.getConsoleSender())
	}

	fun sendOps() {
		forEachOnline { if (isOp) send(this) }
	}

	fun sendNear(location : Location, distance: Int) {
		location.world.players
				.filter { it.location.distance(location) <= distance }
				.forEach { send(it) }
	}

}