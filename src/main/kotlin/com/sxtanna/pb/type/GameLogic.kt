package com.sxtanna.pb.type

import com.sxtanna.pb.user.User

interface GameLogic {

	fun checkStart(): Pair<Boolean, String>

	fun checkEnd()

	fun prepare()


	fun countdown(): Unit

	fun start(): Unit

	fun end(): Unit

	fun reset(): Unit


	fun moveToMap(user: User): Unit

	fun moveToLobby(user: User): Unit

}