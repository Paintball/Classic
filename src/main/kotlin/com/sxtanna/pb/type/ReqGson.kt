package com.sxtanna.pb.type

import com.google.gson.Gson
import com.google.gson.GsonBuilder

interface ReqGson {


	fun gson(pretty : Boolean = true) = if (pretty) gsonPretty else gsonNormal


	companion object {

		private val gsonNormal : Gson = createGson().create()
		private val gsonPretty : Gson = createGson().setPrettyPrinting().create()


		private fun createGson() : GsonBuilder = GsonBuilder().disableHtmlEscaping().enableComplexMapKeySerialization()

	}

}