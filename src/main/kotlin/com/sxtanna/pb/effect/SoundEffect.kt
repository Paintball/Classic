package com.sxtanna.pb.effect

import com.sxtanna.pb.type.Sendable
import org.bukkit.Sound
import org.bukkit.SoundCategory
import org.bukkit.SoundCategory.NEUTRAL
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

data class SoundEffect(override val sending : Sound, val volume: Double = 1.0, val pitch: Double = 1.0, val category : SoundCategory = NEUTRAL) : Sendable<Sound> {

	override fun <C : CommandSender> send(vararg receivers : C) {
		receivers.mapNotNull { it as? Player }.forEach { it.playSound(it.location, sending, category, volume.toFloat(), pitch.toFloat()) }
	}
}