package com.sxtanna.pb.config

data class PaintballConfig(val serverName : String, val access : Access) {

	data class Access(val max : Int = 16, val restricted : Boolean = false, val userPerm : String = "access.{server}")


	companion object {

		val DEFAULT = PaintballConfig("Unknown", Access())

	}

}