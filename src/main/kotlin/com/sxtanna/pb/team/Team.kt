package com.sxtanna.pb.team

import com.sxtanna.pb.base.GameAspect
import com.sxtanna.pb.base.UserState
import com.sxtanna.pb.base.UserState.Offline
import com.sxtanna.pb.game.Game
import com.sxtanna.pb.type.Cleanable
import com.sxtanna.pb.type.ReqGame
import com.sxtanna.pb.user.User
import com.sxtanna.pb.util.Colour
import com.sxtanna.pb.util.Gray

class Team(val name : String, val colour : Colour, override val game : Game) : ReqGame, Cleanable {

	private val users = mutableMapOf<User, UserState>()


	fun chatName() = "${colour.chatColor}$name"

	fun users(inState : UserState? = null) : List<User> {
		val outUsers = users.keys.toMutableList()
		inState?.let { state -> outUsers.removeIf { users[it] != state } }

		return outUsers
	}

	operator fun get(user : User) = users[user] ?: Offline

	operator fun set(user : User, userState : UserState) {
		users[user] = userState
	}


	fun join(user : User, notify : Boolean = true) : Boolean {
		if (user in users) return false

		user.apply {
			GameAspect.Kills.inc()
		}

		this[user] = user.state
		if (notify) user.chat("${Gray}You joined ${chatName()} Team")

		return true
	}

	fun leave(user : User, notify : Boolean = true) : Boolean {
		if (user !in users) return false

		users.remove(user)
		if (notify) user.chat("${Gray}You left ${chatName()} Team")

		return true
	}

	operator fun contains(user : User) = users.contains(user)


	override fun clean() {
		users.clear()
	}


	override fun equals(other : Any?) : Boolean {
		if (this === other) return true
		if (other !is Team) return false

		if (name != other.name) return false
		if (colour != other.colour) return false

		return true
	}

	override fun hashCode() : Int {
		var result = name.hashCode()
		result = 31 * result + colour.hashCode()
		return result
	}

}