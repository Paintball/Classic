package com.sxtanna.pb.kit

import com.sxtanna.pb.base.KitInfo
import com.sxtanna.pb.ext.register
import com.sxtanna.pb.ext.unregister
import com.sxtanna.pb.type.ReqGame
import com.sxtanna.pb.type.Switch
import com.sxtanna.pb.user.User
import org.bukkit.event.Listener

abstract class Kit : Switch, ReqGame, Listener {

	abstract val info: KitInfo


	override fun onEnable() = register(game.plugin)

	override fun onDisable() = unregister()


	operator fun contains(user : User) = game.kitOf(user) == this


	open fun give(user: User) = Unit


	override fun equals(other : Any?) : Boolean {
		if (this !== other || other !is Kit) return false
		if (info != other.info) return false

		return true
	}

	override fun hashCode() : Int = info.hashCode()


}