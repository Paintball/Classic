package com.sxtanna.pb.kit.global

import com.sxtanna.pb.kit.Kit
import com.sxtanna.pb.user.User
import com.sxtanna.pb.util.Colour
import org.bukkit.Material
import org.bukkit.Material.*
import org.bukkit.inventory.ItemFlag.*
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.LeatherArmorMeta

abstract class KitTeamArmour : Kit() {

	override fun give(user : User) {
		super.give(user)

		val colour = game.teamOf(user).colour

		user.player.inventory.apply {
			boots = create(LEATHER_BOOTS, colour)
			helmet = create(LEATHER_HELMET, colour)
			leggings = create(LEATHER_LEGGINGS, colour)
			chestplate = create(LEATHER_CHESTPLATE, colour)
		}
	}


	private fun create(material : Material, colour : Colour) : ItemStack {
		val itemStack = ItemStack(material)
		val meta = itemStack.itemMeta

		meta.apply {
			addItemFlags(HIDE_ATTRIBUTES, HIDE_UNBREAKABLE, HIDE_POTION_EFFECTS)
			spigot().isUnbreakable = true

			(this as LeatherArmorMeta).color = colour.color
		}

		return itemStack.apply { itemMeta = meta }
	}


}