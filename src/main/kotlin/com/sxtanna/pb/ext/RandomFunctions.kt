package com.sxtanna.pb.ext

import java.util.concurrent.ThreadLocalRandom

@JvmName("getInt")
fun int(): Int = ThreadLocalRandom.current().nextInt()

@JvmName("getInt")
fun int(range: Int): Int = ThreadLocalRandom.current().nextInt(range)

@JvmName("getInt")
fun int(min: Int, max: Int): Int = ThreadLocalRandom.current().nextInt(min, max)


@JvmName("getDouble")
fun double(): Double = ThreadLocalRandom.current().nextDouble()

@JvmName("getDouble")
fun double(range: Double): Double = ThreadLocalRandom.current().nextDouble(range)

@JvmName("getDouble")
fun double(min: Double, max: Double): Double = ThreadLocalRandom.current().nextDouble(min, max)


@JvmName("getLong")
fun long(): Long = ThreadLocalRandom.current().nextLong()

@JvmName("getLong")
fun long(range: Long): Long = ThreadLocalRandom.current().nextLong(range)

@JvmName("getLong")
fun long(min: Long, max: Long): Long = ThreadLocalRandom.current().nextLong(min, max)


@JvmName("getBoolean")
fun boolean(): Boolean = ThreadLocalRandom.current().nextBoolean()


fun <E> Array<E>.random(): E? = if (this.isEmpty()) null else if (this.size == 1) this[0] else this[int(0, this.size)]

fun <E> List<E>.random(): E? = if (this.isEmpty()) null else if (this.size == 1) this[0] else this[int(0, this.size)]

fun <E> Set<E>.random(): E? = if (this.isEmpty()) null else if (this.size == 1) this.first() else this.elementAt(int(0, this.size))

fun <K, V> Map<K, V>.random(): Pair<K, V>?  = if (this.isEmpty()) null else if (this.size == 1) this.entries.first().toPair() else this.entries.random()?.toPair()