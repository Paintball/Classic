package com.sxtanna.pb.ext

import com.sxtanna.Kedis
import com.sxtanna.Keury
import com.sxtanna.diss.ScheduledTask
import com.sxtanna.diss.Venture
import com.sxtanna.pb.Paintball
import com.sxtanna.pb.ext.TimeType.Ticks
import com.sxtanna.pb.util.Colour
import org.bukkit.*
import org.bukkit.SoundCategory.NEUTRAL
import org.bukkit.entity.Entity
import org.bukkit.entity.Player
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.material.MaterialData
import org.bukkit.plugin.Plugin
import org.bukkit.scheduler.BukkitRunnable
import java.io.File
import kotlin.reflect.KClass


fun Listener.register(plugin : Plugin) = Bukkit.getPluginManager().registerEvents(this, plugin)

fun Listener.unregister() = HandlerList.unregisterAll(this)


fun Boolean.ifTrue(block: () -> Unit) {
	if (this) block()
}

fun Boolean.ifFalse(block: () -> Unit) {
	if (this.not()) block()
}


fun <R, S : (R.() -> Unit) -> Unit> async(handler : S, block : R.() -> Unit) {
	Venture.schedule {
		handler(block)
	}
}

fun async(task : () -> Unit) {
	Venture.schedule { task() }
}

fun sync(task : BukkitRunnable.() -> Unit) {
	object : BukkitRunnable() {

		override fun run() = task()

	}.runTask(Paintball.instance)
}

fun asyncDelay(delay : Int, timeType : TimeType = Ticks, task : () -> Unit) {
	Venture.schedule(timeType.convert(delay)) { task() }
}

fun syncDelay(delay : Int, task : BukkitRunnable.() -> Unit) {
	object : BukkitRunnable() {

		override fun run() = task()

	}.runTaskLater(Paintball.instance, delay.toLong())
}

fun asyncRepeat(period : Int, delay : Int = 0, timeType : TimeType = Ticks, task : ScheduledTask.() -> Unit) {
	Venture.schedule(timeType.convert(delay), timeType.convert(period), task)
}

fun syncRepeat(period : Int, delay : Int = 0, task : BukkitRunnable.() -> Unit) {
	object : BukkitRunnable() {

		override fun run() = task()

	}.runTaskTimer(Paintball.instance, delay.toLong(), period.toLong())
}


fun <E : Entity> KClass<E>.spawn(location : Location) : E = location.world.spawn(location, this.java)

fun Particle.show(location : Location, amount : Int = 1, offSetX : Double = 0.0, offSetY : Double = 0.0, offSetZ : Double = 0.0, speed : Double = 1.0, data : MaterialData? = null) {
	location.world.spawnParticle(this, location, amount, offSetX, offSetY, offSetZ, speed, data)
}

fun showDust(location : Location, colour : Colour) {
	val color = colour.color

	val (red, green, blue) = Triple(color.red / 255F, (color.green / 255F).toDouble(), (color.blue / 255F).toDouble())
	val x = (if (red == 0F) java.lang.Float.MIN_NORMAL else red).toDouble()


	Particle.REDSTONE.show(location, 0, x, green, blue)
}

fun Sound.play(location : Location, pitch : Double = 1.0, volume : Double = 1.0, category : SoundCategory = NEUTRAL) {
	location.world.playSound(location, this, category, volume.toFloat(), pitch.toFloat())
}


fun forEachOnline(block : Player.() -> Unit) {
	Bukkit.getOnlinePlayers().forEach(block)
}


fun serverDir() : File = File("").absoluteFile

val kedis : Kedis
	get() = Paintball.instance.kedis

val keury : Keury
	get() = Paintball.instance.keury


enum class TimeType(val convert : (Int) -> Int) {

	Ticks({ (it * 50) }), Mills(Int::toInt)

}