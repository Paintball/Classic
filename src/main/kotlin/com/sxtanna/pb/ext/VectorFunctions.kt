package com.sxtanna.pb.ext

import org.bukkit.Location
import org.bukkit.entity.Entity
import org.bukkit.util.Vector
import java.lang.Math.*


fun Vector.toPitch(): Double {
	val x = this.x
	val z = this.z

	return if (x == 0.0 && z == 0.0) {
		if (this.y > 0) -90.0 else 90.0
	}
	else {
		toDegrees(atan(-this.y / sqrt((x * x) + (z * z))))
	}
}

fun Vector.toYaw(): Double {
	return toDegrees((atan2(-this.x, this.z) + (2 * PI)) % (2 * PI))
}


fun direction(location : Location): Vector = location.direction

fun direction(entity : Entity) = direction(entity.location)


fun trace(from: Vector, to: Vector): Vector = from.clone().subtract(to).normalize()

fun trace(from: Location, to: Location) = trace(from.toVector(), to.toVector())

fun trace(from: Entity, to: Entity) = trace(from.location, to.location)


fun distance(from : Vector, to : Vector): Double = from.distance(to)

fun distance(from : Location, to : Location): Double = distance(from.toVector(), to.toVector())

fun distance(from : Entity, to : Entity): Double = distance(from.location, to.location)