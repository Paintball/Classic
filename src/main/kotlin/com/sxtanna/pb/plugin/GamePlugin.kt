package com.sxtanna.pb.plugin

import com.sxtanna.pb.base.GameInfo
import com.sxtanna.pb.game.Game
import com.sxtanna.pb.map.MapProvider
import com.sxtanna.pb.type.Switch

interface GamePlugin<out G: Game> : Switch {

	val game: G

	val info: GameInfo

	val provider: MapProvider


	override fun onEnable() = Unit

	override fun onDisable() = Unit

}