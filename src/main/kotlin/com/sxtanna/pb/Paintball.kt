package com.sxtanna.pb

import com.sxtanna.KUtils.Files
import com.sxtanna.Kedis
import com.sxtanna.Keury
import com.sxtanna.pb.base.GameInfo
import com.sxtanna.pb.config.PaintballConfig
import com.sxtanna.pb.game.classic.ClassicPaintball
import com.sxtanna.pb.map.MapProvider
import com.sxtanna.pb.plugin.GamePlugin
import com.sxtanna.pb.user.UserManager
import org.bukkit.plugin.java.JavaPlugin
import java.io.File

class Paintball : JavaPlugin() {

	lateinit var config : PaintballConfig
		private set

	lateinit var kedis : Kedis
		private set
	lateinit var keury : Keury
		private set


	lateinit var classic : GamePlugin<ClassicPaintball>
		private set


	lateinit var userManager : UserManager
		private set


	override fun onLoad() {
		instance = this

		config = Files.loadOrSave(File(dataFolder, "Config.json"), PaintballConfig.DEFAULT)

		val kedisConfig = Kedis.loadOrCreate(File(dataFolder, "Kedis.json"))
		kedis = Kedis(kedisConfig, 5).apply { start() }

		val keuryConfig = Keury.loadOrCreate(File(dataFolder, "Keury.json"))
		keury = Keury(keuryConfig).apply { start() }
	}

	override fun onEnable() {
		kedis.start()
		keury.start()

		classic = object : GamePlugin<ClassicPaintball> {

			override val game : ClassicPaintball = ClassicPaintball(instance)

			override val info : GameInfo = ClassicPaintball.classicInfo

			override val provider : MapProvider = MapProvider(info, File(dataFolder, "Maps"))

		}

		userManager = UserManager(this).apply { enable() }
	}

	override fun onDisable() {
		if (classic.isEnabled) classic.disable()

		userManager.disable()

		//TODO("Disable everything else")
		kedis.stop()
		keury.stop()
	}


	companion object {

		lateinit var instance : Paintball
			private set

	}

}