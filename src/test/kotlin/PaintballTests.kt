import Thing.*
import com.sxtanna.ext.isIn

fun main(args : Array<String>) {

	val ord = Thing.All.isIn(Some, Most, Few)
	println("Ord is ${ord}")
}

enum class Thing {

	All, Some, Most, Few

}